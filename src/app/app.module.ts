import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// angular material
import { MatToolbarModule } from '@angular/material/toolbar';

// my component
import { UserInfoComponent } from './modules/user-management/user-info.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { LoginComponent } from './core/login/login.component';
import { RegisteryComponent } from './core/registery/registery.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { InsertUpdateChaperComponent } from './modules/insert-update-chaper/insert-update-chaper.component';
import { InsertUpdateTruyenComponent } from './modules/insert-update-truyen/insert-update-truyen.component';
// ng-select
import { NgSelectModule } from '@ng-select/ng-select';
// service
import { BaseService } from './_services/base-service';
import { AuthenticationService } from './_services/authentication.service';
import { LoadDataService } from './_services/load-data.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisteryComponent,
    DashboardComponent,
    InsertUpdateTruyenComponent,
    InsertUpdateChaperComponent,
    UserInfoComponent
  ],
  imports: [
    // angular-material
    MatToolbarModule,

    // angular-material end
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgSelectModule
  ],
  providers: [
    BaseService,
    AuthenticationService,
    LoadDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
