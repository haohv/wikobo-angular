import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './core/login/login.component';
import { RegisteryComponent } from './core/registery/registery.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { InsertUpdateTruyenComponent } from './modules/insert-update-truyen/insert-update-truyen.component';
import { UserInfoComponent } from './modules/user-management/user-info.component';


const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registery', component: RegisteryComponent},
  {path: 'viet-truyen', component: InsertUpdateTruyenComponent},
  {path: 'thong-tin-tai-khoan', component: UserInfoComponent},


  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
