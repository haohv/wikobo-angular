import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountDto } from '../_models/accountDto';
import { HttpHeaders } from '@angular/common/http';
import { BaseService } from './base-service';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'appilication/json'
    })
};

@Injectable({ providedIn: 'root' })
export class AuthenticationService extends BaseService {

    private pathAPI = environment.apiUrl;

    constructor(private http: HttpClient) {
        super();
    };

    login(account: AccountDto): Observable<any> {
        return this.http.post<any>(this.pathAPI + environment.loginUrl, account, super.header()).pipe(
            catchError(super.handleError));
    }

    logout() {
    }
}