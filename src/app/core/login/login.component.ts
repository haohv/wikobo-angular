import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from '../../_services/authentication.service';
import { AccountDto } from 'src/app/_models/accountDto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  accountDto: AccountDto = new AccountDto();
  returnUrl: string;
  error = '';

  selectedItem = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  ngOnInit(): void {
    this.accountDto = new AccountDto();
    this.selectedItem = null;
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    if (this.accountDto) {
      this.authenticationService.login(this.accountDto)
        .subscribe(
          data => {
            console.log(data);
            // this.router.navigate([this.returnUrl]);
            // window.location.reload();
          },
          error => {
            // this.error = error;
            console.log(error);
          });
    }
  }
}
