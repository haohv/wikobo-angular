import { Component, OnInit } from '@angular/core';
import { NavigationLinks, UserLinks } from '../../shared/constant-link';
import { AuthenticationService } from 'src/app/_services/authentication.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  navItems = NavigationLinks;
  userLink = UserLinks;

  isLogin: boolean = false;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.isLogin = localStorage.getItem('currentUser') == null ? false : true;
  }

  logout() {
    this.authenticationService.logout();
  }

}
