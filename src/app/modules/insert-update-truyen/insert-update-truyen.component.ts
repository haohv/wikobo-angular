import { Component, OnInit } from '@angular/core';
import { SelectItem } from '../../_models/select-item';
@Component({
  selector: 'app-insert-update-truyen',
  templateUrl: './insert-update-truyen.component.html',
  styleUrls: ['./insert-update-truyen.component.css']
})
export class InsertUpdateTruyenComponent implements OnInit {
  listLoaiTruyen: SelectItem[];
  listTag: SelectItem[];
  listTinhTrang: SelectItem[];
  listLanguage: SelectItem[];
  listDoiTuong: SelectItem[];

  // selectedItem
  selectedTag: number;

  constructor() { }

  ngOnInit(): void {

    this.listLoaiTruyen = [
      {id: 1, value: 'A'},
      {id: 2, value: 'B'},
      {id: 3, value: 'C'}
    ];
    this.listTag = [
      {id: 1, value: 'A'},
      {id: 2, value: 'B'},
      {id: 3, value: 'C'}
    ];
    this.listTinhTrang = [
      {id: 1, value: 'A'},
      {id: 2, value: 'B'},
      {id: 3, value: 'C'}
    ];
    this.listLanguage = [
      {id: 1, value: 'Tiếng Việt'},
      {id: 2, value: 'Tiếng Anh'},
      {id: 3, value: 'Tiếng Trung'}
    ];

    this.listDoiTuong = [
      {id: 1, value: '< 13 tuổi'},
      {id: 2, value: '13 - 18 tuổi'},
      {id: 3, value: '> 18 tuổi'},
    ];

    this.selectedTag = 1;
  }

}
