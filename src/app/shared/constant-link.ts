/**
 * File: nav-items.ts
 * Author: haohv
 * Description: list navigation link of project
 */
export const NavigationLinks = {
    HOME_PAGE: {
        name: "Trang chủ",
        code: "home",
        link: "/home"
    },

    DANH_MUC: {
        name: "Danh mục",
        code: "danhMuc",
        link: ""
    },

    NGON_TINH: {
        name: "Ngôn tình",
        code: "ngonTinh",
        link: "/ngon-tinh"
    },

    THU_VIEN_SACH: {
        name: "Thư viện sách",
        code: "thuVienSach",
        link: "/thu-vien-sach"
    },

    TRUYEN_VIP: {
        name: "Truyện Vip",
        code: "truyenVip",
        link: "/truyen-vip"
    },

    VIET_TRUYEM: {
        name: "Viết truyện",
        code: "vietTruyen",
        link: "/viet-truyen"
    },
    TRUYEN_CUA_TOI: {
        name: "Truyện của tôi",
        code: "truyenCuaToi",
        link: "/truyen-cua-toi"
    },
    THONG_TIN_TAI_KHOAN: {
        name: "Thông tin TK",
        code: "thongTinTaiKhoan",
        link: "/thong-tin-tai-khoan"
    },
    QUAN_LY_VI: {
        name: "Quản lý ví",
        code: "quanLyVi",
        link: "/quan-ly-vi"
    }
}

export const UserLinks = {
    LOGIN: {
        name: "Đăng nhập",
        code: "login",
        link: "/login"
    },

    REGISTER: {
        name: "Đăng ký",
        code: "register",
        link: "/registery"
    }
}